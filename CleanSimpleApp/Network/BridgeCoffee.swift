//
//  BridgeCoffee.swift
//  simpleApp
//
//  Created by Ringa Sd on 07/04/20.
//  Copyright © 2020 Joao Belmiro. All rights reserved.
//

import Foundation
import Moya


private func JSONResponseDataFormatter(_ data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data)
        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData
    } catch {
        return data // fallback to original data if it can't be serialized.
    }
}


private func JSONResponseDataFormatterToString(_ data: Data) -> String {
    return String(data: JSONResponseDataFormatter(data), encoding: .utf8) ?? "## Cannot map data to String ##"
}
let ServiceProvider = MoyaProvider<BridgeCoffee>(plugins:[NetworkLoggerPlugin.init(configuration: .init(formatter: NetworkLoggerPlugin.Configuration.Formatter(responseData: JSONResponseDataFormatterToString), logOptions: .verbose))])

public enum BridgeCoffee {
    case products
    case session(email: String, password: String)
}

extension BridgeCoffee: TargetType {
    public var baseURL: URL {
        return URL(string: "https://private-anon-3f8ea45396-bridgecoffee.apiary-mock.com")!
    }

    public var path: String {
        switch self {
            case .products: return "/products"
            case .session: return "/sessions"
        }
    }

    public var method: Moya.Method {
        switch self {
            case .products: return .get
            case .session: return .post
        }
    }

    public var sampleData: Data {
        return Data()
    }

    public var task: Task {
        switch self {
            case .products:
              return .requestParameters(
                parameters: [
                    "uid": "1",
                    "authentication_token": "LKskjslK*90s8*&Asjkl"
                ],
                encoding: URLEncoding.default)
            case .session(let email, let password):
                let parameters = ["email" : email,
                              "password": password
                    ]
                return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
        }
        
    }

    public var headers: [String: String]? {
        return ["Content-Type": "application/json"]
    }

    public var validationType: ValidationType {
        return .successCodes
    }
}

//MARK: Helpers
private extension String {
    var utf8Encoded: Data {
    return self.data(using: .utf8)!
  }
}
