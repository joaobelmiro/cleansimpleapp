//
//  CodableResponse.swift
//  simpleApp
//
//  Created by Ringa Sd on 07/04/20.
//  Copyright © 2020 Joao Belmiro. All rights reserved.
//

import Foundation



struct BridgeCoffeeProduct: Codable {
    let status: String
    let data: [Product]
}

struct BridgeCoffeeSession: Codable {
    let status: String
    let data: Session
}
