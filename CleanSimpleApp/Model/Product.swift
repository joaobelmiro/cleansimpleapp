//
//  Product.swift
//  CleanSimpleApp
//
//  Created by Ringa Sd on 16/04/20.
//  Copyright © 2020 Joao Belmiro. All rights reserved.
//

import Foundation

struct Product: Codable {
    var name: String
}
