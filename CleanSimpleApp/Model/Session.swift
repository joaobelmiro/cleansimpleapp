//
//  Session.swift
//  CleanSimpleApp
//
//  Created by Ringa Sd on 17/04/20.
//  Copyright © 2020 Joao Belmiro. All rights reserved.
//

import Foundation


struct Session: Codable {
    var authentication_token: String
}
