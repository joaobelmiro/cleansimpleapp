//
//  LoginModels.swift
//  CleanSimpleApp
//
//  Created by Ringa Sd on 17/04/20.
//  Copyright (c) 2020 Joao Belmiro. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

enum Login
{
  // MARK: Use cases
  
  enum Session
  {
    struct Request
    {
        var email: String
        var password: String
    }
    struct Response
    {
        var authenticated: Bool
    }
    struct ViewModel
    {
        var authenticated: Bool
    }
  }
}
