//
//  TableViewCell.swift
//  CleanSimpleApp
//
//  Created by Ringa Sd on 16/04/20.
//  Copyright © 2020 Joao Belmiro. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
