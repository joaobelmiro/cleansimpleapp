//
//  HomeInteractor.swift
//  CleanSimpleApp
//
//  Created by Ringa Sd on 16/04/20.
//  Copyright (c) 2020 Joao Belmiro. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol HomeBusinessLogic
{
  func checkAuthentication(request: Home.Authentication.Request)
}

protocol HomeDataStore
{
  var userAuthenticationToken: String { get set }
}

class HomeInteractor: HomeBusinessLogic, HomeDataStore
{
  var presenter: HomePresentationLogic?
  var worker: HomeWorker?
  var userAuthenticationToken = ""
  let authenticationToken = "LKskjslK*90s8*&Asjkl"
  
  // MARK: Do something
  
  func checkAuthentication(request: Home.Authentication.Request)
  {
    let isAuthenticated = authenticationToken == userAuthenticationToken
    let response = Home.Authentication.Response(logged: isAuthenticated)
    presenter?.presentAuthentication(response: response)
  }
}
