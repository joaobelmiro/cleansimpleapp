//
//  HomeModels.swift
//  CleanSimpleApp
//
//  Created by Ringa Sd on 16/04/20.
//  Copyright (c) 2020 Joao Belmiro. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

enum Home
{
  // MARK: Use cases
  
  enum Authentication
  {
    struct Request
    {
    }
    struct Response
    {
        var logged: Bool
    }
    struct ViewModel
    {
        var logged: Bool
    }
  }
}
